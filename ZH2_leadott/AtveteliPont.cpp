#include "pch.h"
#include "AtveteliPont.h"
#include <vector>

AtveteliPont::AtveteliPont(string nev_)
{
	_atveteliPontNeve = nev_;
}

AtveteliPont::~AtveteliPont()
{
}

string AtveteliPont::GetAtveteliPontNeve() const
{
	return _atveteliPontNeve;
}

void AtveteliPont::Leadas(SzamitastechnikaiEszkoz* eszkoz_)
{
	cout << "Hany oran keresztul volt hasznalva az eszkoz?" << endl;
	int hasznalatiIdo;
	cin >> hasznalatiIdo;

	Monitor* tmpMonitor = dynamic_cast<Monitor*>(eszkoz_);
	Projektor* tmpProjektor = dynamic_cast<Projektor*>(eszkoz_);
	Laptop* tmpLaptop = dynamic_cast<Laptop*>(eszkoz_);

	if (tmpMonitor)
	{
		tmpMonitor->SetFutottOra(hasznalatiIdo);
		cout << "Leadva a " + _atveteliPontNeve + " helyen, eszkozhasznalati ido: " + to_string(hasznalatiIdo);
		_kolcsonozhetoMonitorok.push(tmpMonitor);

		ITisztithato* tmpEszkoz = dynamic_cast<ITisztithato*>(tmpMonitor);

		if (tmpEszkoz)
		{
			tmpEszkoz->Tisztit();
		}
	}
	if (tmpProjektor)
	{
		tmpProjektor->SetFutottOra(hasznalatiIdo);
		cout << "Leadva a " + _atveteliPontNeve + " helyen, eszkozhasznalati ido: " + to_string(hasznalatiIdo);
		_kolcsonozhetoProjektorok.push(tmpProjektor);

		ITisztithato* tmpEszkoz = dynamic_cast<ITisztithato*>(tmpProjektor);

		if (tmpEszkoz)
		{
			tmpEszkoz->Tisztit();
		}
	}
	if (tmpLaptop)
	{
		tmpLaptop->SetFutottOra(hasznalatiIdo);
		cout << "Leadva a " + _atveteliPontNeve + " helyen, eszkozhasznalati ido: " + to_string(hasznalatiIdo);
		_kolcsonozhetoLaptopok.push(tmpLaptop);

		ITisztithato* tmpEszkoz = dynamic_cast<ITisztithato*>(tmpLaptop);

		if (tmpEszkoz)
		{
			tmpEszkoz->Tisztit();
		}
	}
}

Monitor* AtveteliPont::Felvetel(Monitor*)
{
	Monitor* felvettMonitor = new Monitor();
	if (!_kolcsonozhetoMonitorok.empty())
	{
		//felvettMonitor = _kolcsonozhetoMonitorok.pop();
	}
	//cout << "#ID:" + felvettMonitor->GetID() + "kik�lcs�n�zve: " + to_string(felvettMonitor->GetFutottOra()) + " �ra haszn�lati id�vel";

	return felvettMonitor;
}
