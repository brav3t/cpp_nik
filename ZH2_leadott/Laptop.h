#pragma once
#include "SzamitastechnikaiEszkoz.h"
#include "ITisztithato.h"

class Laptop :
	public SzamitastechnikaiEszkoz, public ITisztithato
{
public:
	Laptop();
	~Laptop();
	virtual void Tisztit();

private:
	int _utolsoTisztitasOtaElteltIdo;
};
