#include "pch.h"
#include "SzamitastechnikaiEszkoz.h"

static int ID = 0;

SzamitastechnikaiEszkoz::SzamitastechnikaiEszkoz()
{
	_id = ID;
	ID++;
}


SzamitastechnikaiEszkoz::~SzamitastechnikaiEszkoz()
{
}

std::string SzamitastechnikaiEszkoz::GetID() const
{
	return _id;
}

int SzamitastechnikaiEszkoz::GetFutottOra() const
{
	return _futottOra;
}

void SzamitastechnikaiEszkoz::SetFutottOra(int hasznalatiIdo_)
{
	_futottOra += hasznalatiIdo_;
}

AtveteliPont * SzamitastechnikaiEszkoz::GetLeadasHelye() const
{
	return _leadasHelye;
}
