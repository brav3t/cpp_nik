#pragma once
#include <string>

class AtveteliPont;

class SzamitastechnikaiEszkoz
{
public:
	SzamitastechnikaiEszkoz();
	virtual ~SzamitastechnikaiEszkoz();
	std::string GetID() const;
	int GetFutottOra() const;
	void SetFutottOra(int hasznalatiIdo_);
	AtveteliPont* GetLeadasHelye() const;

private:
	AtveteliPont* _leadasHelye;

protected:
	std::string _id;
	int _futottOra;

};
