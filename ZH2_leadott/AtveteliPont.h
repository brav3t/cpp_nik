#pragma once
#include <string>
#include <stack>
#include <deque>
#include <queue>
#include "Projektor.h"
#include "Laptop.h"
#include "Monitor.h"

using namespace std;

class AtveteliPont
{
public:
	AtveteliPont(string nev_);
	~AtveteliPont();
	string GetAtveteliPontNeve() const;
	void Leadas(SzamitastechnikaiEszkoz* eszkoz_);
	Monitor* Felvetel(Monitor*);
	Projektor* Felvetel(Projektor*);
	Laptop* Felvetel(Laptop*);

private:
	string _atveteliPontNeve;
	stack<Projektor*> _kolcsonozhetoProjektorok;
	queue<Monitor*> _kolcsonozhetoMonitorok;
	stack<Laptop*> _kolcsonozhetoLaptopok;
};
