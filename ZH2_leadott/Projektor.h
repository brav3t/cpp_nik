#pragma once
#include "SzamitastechnikaiEszkoz.h"
#include "ITisztithato.h"
#include <iostream>

class Projektor :
	public SzamitastechnikaiEszkoz, public ITisztithato
{
public:
	Projektor();
	~Projektor();
	virtual void Tisztit();

private:
	int _utolsoTisztitasOtaElteltIdo;
};
