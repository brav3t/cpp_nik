#include "pch.h"
#include <iostream>
#include <vector>
#include <map>
#include "SzamitastechnikaiEszkoz.h"
#include "Monitor.h"
#include "Laptop.h"
#include "Projektor.h"
#include "ITisztithato.h"
#include "AtveteliPont.h"

int main()
{
	Monitor* m1 = new Monitor();
	Laptop* l1 = new Laptop();
	Projektor* p1 = new Projektor();

	vector<SzamitastechnikaiEszkoz*> v;
	v.push_back(m1);
	v.push_back(l1);
	v.push_back(p1);

	AtveteliPont pont("LaptopSarok");
	pont.Leadas(m1);
	pont.Felvetel(l1);

	delete m1;
	delete l1;
	delete p1;
}
