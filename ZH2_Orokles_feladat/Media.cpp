#include "pch.h"
#include "Media.h"

static int ID = 0;

Media::Media()
{
	_id = ID;
	ID++;

}

Media::~Media()
{
	delete _location;
}

int Media::GetId()
{
	return _id;
}

void Media::SetLocation(Storage * location_)
{
	_location = location_;
}
