#pragma once
#include "Storage.h"

class Storage;

class Media
{
public:
	Media();
	virtual ~Media();
	int GetId();
	void SetLocation(Storage* location_);
	
private:
	int _id;
	Storage* _location;

public:

};

