#pragma once
#include <deque>
#include "Media.h"

using namespace std;

class Media;

class Storage
{
public:
	void PutOn(Media* media_);
	//void TakeOff(Media* media_);

private:
	deque<Media*> _items;
};

