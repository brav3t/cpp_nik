#pragma once
#include "Media.h"
#include "ICategory.h"

class Ebook :
	public Media,
	public ICategory
{
public:
	Ebook(Category category_) : _category(category_) {};
	~Ebook();
	virtual Category GetCategory() const;

private:
	Category _category;
};

