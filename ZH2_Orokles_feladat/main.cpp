#include "pch.h"
#include <iostream>
#include <string>
#include <vector>
#include <map>
#include "Media.h"
#include "Ebook.h"
#include "Journal.h"
#include "MusicPlaylist.h"
#include "Movie.h"

using namespace std;

map<int, Media*>* GetByCategory(Category category_, vector<Media*>& vector_) 
{
	map<int, Media*>* tempMap = new map<int, Media*>();
	for (vector<Media*>::const_iterator it = vector_.begin(); it != vector_.end(); it++) 
	{
		ICategory* temp = dynamic_cast<ICategory*>(*it);
		if (temp && temp->GetCategory() == category_)
		{
			tempMap->emplace(pair<int, Media*>((*it)->GetId(), *it));
		}
	}
	return tempMap;
}

int main()
{		
	Media* m1 = new Media();
	Media* e1 = new Ebook(Category::Crime);
	Media* e2 = new Ebook(Category::Historical);
	Media* e3 = new Ebook(Category::Professional);
	Media* e4 = new Ebook(Category::Scientific);
	Media* j1 = new Journal();
	Media* mov1 = new Movie();
	Media* mpl1 = new MusicPlaylist();

	vector<Media*>* v = new vector<Media*>();
	v->push_back(m1);
	v->push_back(e1);
	v->push_back(e2);
	v->push_back(e3);
	v->push_back(e4);
	v->push_back(j1);
	v->push_back(mov1);
	v->push_back(mpl1);
	
	map<int, Media*>* m = GetByCategory(Category::Scientific, *v);
	
	for (map<int, Media*>::const_iterator it = m->begin(); it != m->end(); it++)
	{
		cout << it->first << endl;
	}

	delete m1;
	delete e1;
	delete e2;
	delete e3;
	delete e4;
	delete j1;
	delete mov1;
	delete mpl1;
	delete v;
}
