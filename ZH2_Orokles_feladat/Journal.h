#pragma once
#include "Media.h"
#include "ICategory.h"

class Journal :
	public Media,
	public ICategory
{
public:
	Journal();
	~Journal();
	virtual Category GetCategory() const;

private:
	Category _category;
};

