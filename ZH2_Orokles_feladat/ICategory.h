#pragma once

enum Category {Crime, Historical, Professional, Scientific};

class ICategory
{
public:
	virtual Category GetCategory() const = 0;
};

