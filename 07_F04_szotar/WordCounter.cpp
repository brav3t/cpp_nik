#include "pch.h"
#include "WordCounter.h"

WordCounter::WordCounter()
{
	_dictionary = new map<string, vector<string> >();
}

WordCounter::~WordCounter()
{
	_dictionary->clear();
	delete _dictionary;
}

string WordCounter::GetSubWord(string& number_, int& start_, int end_)
{
	string subword;

	for (size_t i = start_; i <= end_; i++)
	{
		subword += number_[i];
	}

	return subword;
}

void WordCounter::GetWords(string & number_)
{
	int numberLength = number_.length();

	vector<string> resultVector;
	string subWord;
	int startOfWord = 0;

	for (int actualWordLength = 0; actualWordLength < numberLength; actualWordLength++)
	{
		while (startOfWord + actualWordLength < numberLength)
		{
			subWord = GetSubWord(number_, startOfWord, startOfWord + actualWordLength);
			if (_dictionary->count(subWord) > 0)
			{
				vector<string>* tempVector = &_dictionary->find(subWord)->second;
				resultVector.insert(resultVector.end(), tempVector->begin(), tempVector->end());
			}
			startOfWord++;
		}
		startOfWord = 0;
	}

	if (!resultVector.empty())
	{
		for (vector<string>::const_iterator it = resultVector.begin(); it != resultVector.end(); it++)
		{
			cout << *it << endl;
		}

		cout << "number of words: " + to_string(resultVector.size()) << endl;
	}
	else
	{
		cout << "No words with code: " + number_ + " in the dictionary" << endl;
	}
}

void WordCounter::PopulateDictionary(string inputFile_)
{
	ifstream inputFile(inputFile_);

	string word;
	string keyOfWord;
	
	//while (!inputFile.eof())

	while (getline(inputFile, word))
	{
		for (size_t i = 0; i < word.length(); i++)
		{
			keyOfWord += _keypad[word[i]];
		}

		if (_dictionary->count(keyOfWord) == 0)
		{
			vector<string>* v = new vector<string>();
			v->push_back(word);
			_dictionary->insert(pair<string, vector<string> >(keyOfWord, *v));
		}
		else
		{
			_dictionary->find(keyOfWord)->second.push_back(word);
		}

		keyOfWord = "";
	}
	cout << "Dictionary uploaded" << endl;
}

ostream & operator<<(ostream & lhs_, const WordCounter& rhs_)
{
	for (map<string, vector<string> >::iterator it = rhs_._dictionary->begin(); it != rhs_._dictionary->end(); it++) 
	{
		for (vector<string>::const_iterator vit = it->second.begin(); vit != it->second.end(); vit++)
		{
			lhs_ << *vit + " : " + it->first << endl;
		}
	}
	return lhs_;
}
