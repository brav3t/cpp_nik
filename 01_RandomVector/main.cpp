#include "pch.h"
#include <iostream>
#include <cstdlib>
#include <ctime>
#include <cmath>
#include <vector>
#include <string>

using namespace std;

const vector< int > ArrayGenerate(const int& n_)
{
	srand(time(NULL));
	vector< int > a;
	for (int i = 0; i < n_; i++)
	{
		a.push_back(rand() % 100 + 1);
	}
	return a;
}

void ToConsole(const vector< int >& a_)
{
	string sep = "";
	for (vector<int>::const_iterator it = a_.begin(); it != a_.end(); it++)
	{
		cout << sep << *it;
		sep = ", ";
	}
	cout << endl;
}

const bool IsDivisible(const int& num_, const int& divisor_)
{
	return num_ % divisor_ == 0;
}


const bool IsPrime(const int& num_)
{
	int i = 2;
	const double sqrtNum = sqrt(num_);
	const double eps = 1e-6;
	while (i <= sqrtNum + eps && !IsDivisible(num_, i))
	{
		i++;
	}
	return i > sqrtNum + eps;
}

const int NumberOfPrimes(const vector< int >& a_)
{
	int counter = 0;
	for (vector<int>::const_iterator it = a_.begin(); it != a_.end(); it++)
	{
		counter += IsPrime(*it) ? 1 : 0;
	}
	return counter;
}

int main()
{
	cout << "Number of array elements: ";
	int numberOfElements;
	cin >> numberOfElements;
	const vector< int > myVector = ArrayGenerate(numberOfElements);
	ToConsole(myVector);
	cout << "Number of primes: " << NumberOfPrimes(myVector) << endl;
}
