#pragma once
#include <string>

using namespace std;

namespace zh
{
	class ParserException
	{
	public:
		ParserException(const string& message_) : _message(message_) {};
		string getMessage() const { return _message + " cannot be parsed to Ajandek enum"; };
	private:
		string _message;
	};

	class AjandekTipus
	{
	public:
		enum Ajandek
		{
			kave,
			bor,
			pezsgo,
			csokolade,
			desszert,
			kezkrem,
			tusfurdo
		};

		AjandekTipus(const Ajandek& ajandek_) : _ajandek(ajandek_) {};
		Ajandek getAjandek() const { return _ajandek; };

		static AjandekTipus Parse(const string& szo_)
		{
			if (szo_ == "Kave")
				return AjandekTipus(kave);
			else if (szo_ == "Bor")
				return AjandekTipus(bor);
			else if (szo_ == "Pezsgo")
				return AjandekTipus(pezsgo);
			else if (szo_ == "Csokolade")
				return AjandekTipus(csokolade);
			else if (szo_ == "Desszert")
				return AjandekTipus(desszert);
			else if (szo_ == "Kezkrem")
				return AjandekTipus(kezkrem);
			else if (szo_ == "Tusfurdo")
				return AjandekTipus(tusfurdo);
			else
				throw ParserException(szo_);
		}

		operator string() const {
			switch (_ajandek)
			{
			case AjandekTipus::kave:
				return "kave";
				break;
			case AjandekTipus::bor:
				return "bor";
				break;
			case AjandekTipus::pezsgo:
				return "pezsgo";
				break;
			case AjandekTipus::csokolade:
				return "csokolade";
				break;
			case AjandekTipus::desszert:
				return "desszert";
				break;
			case AjandekTipus::kezkrem:
				return "kezkrem";
				break;
			case AjandekTipus::tusfurdo:
				return "tusfurdo";
				break;
			default:
				break;
			}
		}

	private:
		Ajandek _ajandek;
	};
}
