#pragma once
#include "AjandekMennyisegPar.h"
#include <string>
#include <vector>

using namespace std;

namespace zh
{
	class AjandekKosar
	{
	public:
		AjandekKosar(const string& ujKosarNev_) : _kosarNev(ujKosarNev_) {};
		void UjAjandek(const AjandekMennyisegPar& ujAjandekKosarba_);

		vector<AjandekMennyisegPar> getKosar() const { return _ajandekokKosarban; };
		string getKosarNev() const { return _kosarNev; };

		friend ostream& operator<<(ostream& lhs_, const AjandekKosar& rhs_);

	private:
		string _kosarNev;
		vector<AjandekMennyisegPar> _ajandekokKosarban;
	};
}