#include "pch.h"
#include "AjandekKosarKatalogus.h"
#include <fstream>

namespace zh
{
	void AjandekKosarKatalogus::ujAjandekKosar(const AjandekKosar& kosar_)
	{
		_katalogus.push_back(kosar_);
	}

	AjandekKosar& AjandekKosarKatalogus::Keres(const string & keresettKosar_)
	{
		for (vector<AjandekKosar>::iterator it = _katalogus.begin(); it != _katalogus.end(); it++)
		{
			if (it->getKosarNev() == keresettKosar_)
			{
				return *it;
			}
		}
		throw NemTalaltKosarKivetel(keresettKosar_ + " nem található");
	}

	void AjandekKosarKatalogus::Beolvas(const string & ajandekKosarak_)
	{
		fstream inputFile(ajandekKosarak_);
		string line;
		string kosarNev;
		while (!inputFile.eof())
		{
			getline(inputFile, line);
			size_t pos = line.find('(');
			if (pos == string::npos)
			{
				AjandekKosar kosar(line);
				_katalogus.push_back(kosar);
				kosarNev = line;
			}
			else
			{
				AjandekKosar& kosar = Keres(kosarNev);
				try
				{
					kosar.UjAjandek(AjandekMennyisegPar(AjandekTipus::Parse(line.substr(0, pos)), stoi(line.substr(pos + 1, 1))));
				}
				catch (ParserException e_)
				{
					cout << e_.getMessage();
				}
			}
		}
	}

	ostream & operator<<(ostream & lhs_, const AjandekKosarKatalogus & rhs_)
	{
		for (vector<AjandekKosar>::const_iterator it = rhs_._katalogus.begin(); it != rhs_._katalogus.end(); it++)
		{
			lhs_ << *it;
		}
		return lhs_;
	}
}
