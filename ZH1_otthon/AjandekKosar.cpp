#include "pch.h"
#include "AjandekKosar.h"

namespace zh
{
	void AjandekKosar::UjAjandek(const AjandekMennyisegPar & ujAjandekKosarba_)
	{
		_ajandekokKosarban.push_back(ujAjandekKosarba_);
	}

	ostream & operator<<(ostream & lhs_, const AjandekKosar & rhs_)
	{
		lhs_ << rhs_._kosarNev << endl;

		for (vector<AjandekMennyisegPar>::const_iterator it = rhs_._ajandekokKosarban.begin(); it != rhs_._ajandekokKosarban.end(); it++)
		{
			lhs_ << *it << endl;
		}

		return lhs_;
	}
}
