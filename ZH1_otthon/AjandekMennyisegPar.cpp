#include "pch.h"
#include "AjandekMennyisegPar.h"

namespace zh
{
	ostream & operator<<(ostream & lhs_, const AjandekMennyisegPar & rhs_)
	{
		lhs_ << string(rhs_._ajandek) << "(" << rhs_._mennyiseg << ")";
		return lhs_;
	}
}
