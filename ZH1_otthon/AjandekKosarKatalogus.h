#pragma once
#include "AjandekKosar.h"
#include <string>
#include <iostream>

using namespace std;

namespace zh
{
	class NemTalaltKosarKivetel
	{
	public:
		NemTalaltKosarKivetel(const string& message_) :_message(message_) {};
		string getMessage() const { return _message; };
	private:
		string _message;
	};

	class AjandekKosarKatalogus
	{
	public:
		void ujAjandekKosar(const AjandekKosar& kosar_);
		void Beolvas(const string& ajandekKosarak_);
		AjandekKosar& Keres(const string& keresettKosar_);
		vector<AjandekKosar> getKatalogus() const { return _katalogus; };

		friend ostream& operator<<(ostream& lhs_, const AjandekKosarKatalogus& rhs_);

	private:
		vector<AjandekKosar> _katalogus;
	};
}

