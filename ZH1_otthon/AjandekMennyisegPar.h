#pragma once
#include "AjandekTipus.h"

namespace zh
{
	class AjandekMennyisegPar
	{
	public:
		AjandekMennyisegPar(const AjandekTipus& ajandek_, const int mennyiseg_) : _ajandek(ajandek_), _mennyiseg(mennyiseg_) {};

		AjandekTipus getAjandekTipus() const { return _ajandek; };
		int getMennyiseg() const { return _mennyiseg; };

		friend ostream& operator<<(ostream& lhs_, const AjandekMennyisegPar& rhs_);

	private:
		AjandekTipus _ajandek;
		int _mennyiseg;
	};
}

