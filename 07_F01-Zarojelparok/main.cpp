#include "pch.h"
#include <iostream>
#include <string>
#include <stack>
#include <vector>
#include <algorithm>
#include <map>


bool isJolZarojelezett(const std::string& s_)
{
	std::stack<char> verem;
	std::vector<char> nyitoZarojelek = { '(', '[', '{' };
	std::vector<char> zaroZarojelek = { ')', ']', '}' };
	std::map<char, char> osszetartozoZarojelek;
	for (size_t i = 0; i < nyitoZarojelek.size(); i++)
	{
		osszetartozoZarojelek.emplace(
			std::make_pair(nyitoZarojelek[i], zaroZarojelek[i]));
	}

	for (std::string::const_iterator it = s_.begin();
		it != s_.end();
		it++)
	{
		char aktualisKarakter = *it;
		std::vector<char>::iterator findResult =
			std::find(nyitoZarojelek.begin(),
				nyitoZarojelek.end(), aktualisKarakter);
		if (findResult != nyitoZarojelek.end()) //ha nyitózárójelem van
		{
			verem.push(aktualisKarakter);
			continue;
		}

		findResult =
			std::find(zaroZarojelek.begin(),
				zaroZarojelek.end(), aktualisKarakter);
		if (findResult != zaroZarojelek.end()) //ha zárójelem van
		{
			if (!verem.empty() &&
				osszetartozoZarojelek[verem.top()] == aktualisKarakter)
			{
				verem.pop();
			}
			else
			{
				return false;
			}
		}
	}

	return verem.empty();
}

int main()
{
	std::string s = "2*{3+[arccos(x+4)-(4x+8)^4]*67+(42-39)/4}-47";
	std::cout << (isJolZarojelezett(s) ? "OK" : "Nem OK") << std::endl;
}
