#pragma once
#include "Teglalap.h"

class Negyzet : public Teglalap
{
public:
	Negyzet(const std::string& szin_, double oldalHossz_);
	void setMagassag(double);
	void setSzelesseg(double);

private:
	void setOldalHossz(double);
	std::string toString() const;
};